# Yet Another i3 Python Status Bar (yai3psb)

## Another i3 status bar write in Python

[List of powerline separators](https://github.com/ryanoasis/powerline-extra-symbols)

## List of blocks

- Audio
- Battery
- Cpu Usage
- Custom Script
- Date
- Disk Usage
- Focused Window
- Music
- Network
- Text

## TODO

### Quality

- [ ] Documentation / Wiki
- [ ] PEP-8
- [ ] CI/CD
- [ ] Unit Tests
- [ ] Setuptools (setup.py + requirements.txt)

### Improvements

- [x] Themes Colors
- [x] Error Block
- [x] Block instanciation
- [x] Space around text in blocks
- [x] Separators instanciation
- [x] Argument parse
- [ ] Timeout for custom cmd
- [ ] Time interval?
- [ ] CPU usage (multiple cast, same tests, yet resolved loop, ...)
- [x] Warning/ Critical
- [x] Override warning/crit back/foreground if back/foreground is set
- [x] Multiblocks
- [x] Open config file only one time
- [x] Try/Except for get_val and on_click instead of having all in blocks
- [ ] better launch_event method (factorize?)
- [ ] First event click
- [x] No extra space around multiblocks
- [ ] Custom icon for blocks
- [x] Multiple Same block in config
- [ ] Make over (custom click) opt by block not global

### util module

- [x] Rolling text
- [x] Launch Command
- [x] Get command
- [x] Time interval
- [ ] µs to string

### Block improvements

- [ ] Configurable criticity level
- [ ] Fixed width to prevent mooving bar

- Music

- [ ] Get album info
- [ ] Player volume Control
- [x] Rolling text for long string
- [x] Use Can... to not display not usable buttons
- [x] Dbus-send to read info instead of qdbus
- [x] Dedicated methods to use dbus-send

- Date

- [ ] Date Block (use locale day/month names, choose format)

- Network

- [ ] Choose info to print (ip, type, domain, connection, dns, etc.)
- [ ] Rolling text
- [ ] Multiblock by interface
- [ ] Add other driver (ip, ifconfig)

- CPU

- [ ] Load icon (low, mid, full)
- [ ] Criticity

- Disk usage

- [ ] Criticity

### New Blocks

- [x] Current Window
- [x] Network
- [x] Disk
- [x] CPU
- [ ] Temp
- [ ] Memory
- [ ] Weather (curl wttr.in) https://github.com/chubin/wttr.in#one-line-output
- [ ] Keyboard
- [x] cmus / MediaPlayer2 protocol
- [ ] Xrandr
- [ ] GPU
