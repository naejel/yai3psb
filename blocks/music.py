#!/usr/bin/python3

import shlex, subprocess
import time
import util

class Music:
    def __init__(self, config):
        self.config = config
        self.output_format = config['output_format']
        self.error = None
        self.interface = None
        self.to_roll = {}
        self.interface_list = None
        self.first_block = config['multiblocks'][0]
        self.get_interface()

    def get_interface(self):
        try:
            util.launch_command('qdbus')
        except:
            self.error = 'Cannot launch qdbus'
            self.interface_list = None
            return
        try:
            interface_list = util.get_command("qdbus | grep MediaPlayer2",' ',True)
            if isinstance(self.config['interface'], int):
                if self.config['interface'] <= len(interface_list):
                    self.interface = interface_list[self.config['interface']]
                else:
                    self.interface = interface_list[-1]
            else:
                for interface in interface_list:
                    if self.config['interface'] in interface:
                        self.interface = interface
                    else:
                        self.interface = interface_list[0]
            self.interface_list = interface_list
        except:
            self.interface_list = None
            self.interface = None

    def dbus_get(self, interface: str, to_get: str) -> str:
        cmd = "dbus-send --print-reply --type=method_call \
                --dest=" + interface + " /org/mpris/MediaPlayer2 \
                org.freedesktop.DBus.Properties.Get \
                string:'org.mpris.MediaPlayer2.Player' \
                string:'" + to_get + "'"
        return util.get_command(cmd)

    def dbus_get_all(self, interface: str) -> str:
        cmd = "dbus-send --print-reply --type=method_call \
                --dest=" + interface + " /org/mpris/MediaPlayer2 \
                org.freedesktop.DBus.Properties.GetAll \
                string:'org.mpris.MediaPlayer2.Player'"
        return util.get_command(cmd)

    def dbus_set(self, interface: str, to_set: str):
        cmd = "dbus-send --print-reply \
                --dest=" + interface + " /org/mpris/MediaPlayer2 \
                    org.mpris.MediaPlayer2.Player." + to_set
        util.launch_command(cmd)

    def dbus_set_method(self, interface: str, method: str, args: str):
        cmd = "dbus-send --print-reply --type=method_call \
                --dest=" + interface + " /org/mpris/MediaPlayer2 \
                    org.freedesktop.DBus.Properties.Set \
                    string:'org.mpris.MediaPlayer2.Player' \
                    string:\'" + method + "\' " + args
        util.launch_command(cmd)

    def µs_to_str(self, µs: int) -> str:
        seconds = µs / 1000000
        min, sec = divmod(seconds, 60)
        hour, min = divmod(min, 60)
        if hour != 0:
            return "%d:%02d:%02d" % (hour, min, sec)
        else:
            return "%02d:%02d" % (min, sec)

    def roll_text(self, to_roll: str) -> str:
        if to_roll not in self.to_roll:
            roll_tuple = util.RollingText(to_roll, 0, self.config['max_width'], 0, self.config['roll_interval'])
            self.to_roll.update({to_roll: roll_tuple})
        to_print, last_pos, tick = util.roll_text(self.to_roll[to_roll])
        roll_tuple = util.RollingText(to_roll, last_pos, self.config['max_width'], tick, self.config['roll_interval'])
        self.to_roll.update({to_roll: roll_tuple})
        return to_print

    def get_all_infos(self):
        self.repeat = None
        self.shuffle = None
        self.play = None
        self.cangonext = None
        self.cangoprev = None
        self.canplay = None
        self.canpause = None
        self.cancontrol = None
        self.position = None
        self.length = None
        self.artist = None
        self.song = None
        self.album = None
        self.album_artist = None

        if self.interface:
            ret = self.dbus_get_all(self.interface)
            for line in ret:
                if 'PlaybackStatus' in line:
                    if 'Playing' in ret[ret.index(line)+1]:
                        self.play = True
                    else:
                        self.play = False
                elif 'LoopStatus' in line:
                    if 'Track' in ret[ret.index(line)+1]:
                        self.repeat = 'Track'
                    elif 'Playlist' in ret[ret.index(line)+1]:
                        self.repeat = 'Playlist'
                    elif 'None' in ret[ret.index(line)+1]:
                        self.repeat = 'None'
                    else:
                        self.repeat = None
                elif 'Shuffle' in line:
                    if 'true' in ret[ret.index(line)+1]:
                        self.shuffle = True
                    else:
                        self.shuffle = False
                elif 'Position' in line:
                    self.position = int(ret[ret.index(line)+1].split('int64')[1])
                elif 'CanGoNext' in line:
                    if 'true' in ret[ret.index(line)+1]:
                        self.cangonext = True
                    else:
                        self.cangonext = False
                elif 'CanGoPrevious' in line:
                    if 'true' in ret[ret.index(line)+1]:
                        self.cangoprev = True
                    else:
                        self.cangoprev = False
                elif 'CanPlay' in line:
                    if 'true' in ret[ret.index(line)+1]:
                        self.canplay = True
                    else:
                        self.canplay = False
                elif 'CanPause' in line:
                    if 'true' in ret[ret.index(line)+1]:
                        self.canpause = True
                    else:
                        self.canpause = False
                elif 'CanControl' in line:
                    if 'true' in ret[ret.index(line)+1]:
                        self.cancontrol = True
                    else:
                        self.cancontrol = False
                elif 'Metadata' in line:
                    for subindex in range(ret.index(line), len(ret)):
                        if 'artist' in ret[subindex]:
                            self.artist = ret[subindex + 2].split("\"")[1]
                        elif 'albumArtist' in ret[subindex]:
                            self.album_artist = ret[subindex + 2].split("\"")[1]
                        elif 'title' in ret[subindex]:
                            self.song = ret[subindex + 1].split("\"")[1]
                        elif 'length' in ret[subindex]:
                            self.length = int(ret[subindex + 1].split("int64")[1])
                else:
                    pass
        else:
            self.get_interface()

    def set_shuffle(self, to: bool):
        if self.interface:
            to_send = "variant:boolean:" + str(to).lower()
            self.dbus_set_method(self.interface, 'Shuffle', to_send)

    def set_repeat(self, to: str):
        if self.interface:
            to_send = "variant:string:" + to.capitalize()
            self.dbus_set_method(self.interface, 'LoopStatus', to_send)

    def on_click_icon(self, event: dict):
        pass

    def on_click_player(self, event: dict):
        if event['button'] == 1:
            self.artist = None
            self.song = None
            current_interface = self.interface
            self.get_interface()
            if self.interface_list:
                index = self.interface_list.index(current_interface)
                if index < len(self.interface_list) - 1:
                    self.interface = self.interface_list[index + 1]
                else:
                    self.interface = self.interface_list[0]

    def on_click_song(self, event: dict):
        pass

    def on_click_artist(self, event: dict):
        pass

    def on_click_duration(self, event: dict):
        pass

    def on_click_position(self, event: dict):
        pass

    def on_click_time_left(self, event: dict):
        pass

    def on_click_prev(self, event: dict):
        if event['button'] == 1:
            if self.interface:
                if self.cangoprev:
                    self.to_roll = {}
                    self.song = None
                    self.artist = None
                    self.album_artist = None
                    self.dbus_set(self.interface, 'Previous')

    def on_click_play(self, event: dict):
        if event['button'] == 1:
            if self.interface:
                if self.canplay is True:
                    self.dbus_set(self.interface, 'PlayPause')

    def on_click_next(self, event: dict):
        if event['button'] == 1:
            if self.interface:
                if self.cangonext is True:
                    self.to_roll = {}
                    self.song = None
                    self.artist = None
                    self.album_artist = None
                    self.dbus_set(self.interface, 'Next')

    def on_click_repeat(self, event: dict):
        if event['button'] == 1:
            if self.interface:
                if self.cancontrol is True:
                    if self.repeat == 'Track' or self.repeat == 'None':
                        self.set_repeat('Playlist')
                        self.set_shuffle(False)
                    elif self.repeat == 'Playlist' and self.shuffle is False:
                        self.set_shuffle(True)
                    elif self.shuffle is True and self.repeat == 'Playlist':
                        self.set_shuffle(False)
                        self.set_repeat('Track')

    def get_icon(self) -> tuple:
        if self.first_block == 'icon':
            self.get_all_infos()
        if self.output_format == 'icon':
            return("idle", "\uF001")
        else:
            return("idle", 'Þ')

    def get_sep_block(self) -> tuple:
        if self.first_block == 'sep_block':
            self.get_all_infos()
        return("idle", self.config['sep_block'])

    def get_player(self):
        if self.first_block == 'player':
            self.get_all_infos()
        if self.interface:
            return("idle", self.interface.split('.')[3])
        return(None, None)

    def get_song(self):
        if self.first_block == 'song':
            self.get_all_infos()
        if self.song:
            to_print = self.roll_text(self.song)
            return ("idle", to_print)
        else:
            return(None, None)

    def get_artist(self):
        if self.first_block == 'artist':
            self.get_all_infos()
        if self.artist:
            to_print = self.roll_text(self.artist)
            return ("idle", to_print)
        return(None, None)

    def get_duration(self):
        if self.first_block == 'duration':
            self.get_all_infos()
        if self.length:
            return("idle", self.µs_to_str(self.length))
        return (None, None)

    def get_time_left(self):
        if self.first_block == 'time_left':
            self.get_all_infos()
        if self.length:
            if self.position:
                return("idle", '-' + self.µs_to_str(self.length - self.position))
        return (None, None)

    def get_elapsed(self):
        if self.first_block == 'elapsed':
            self.get_all_infos()
        if self.position:
            return("idle", self.µs_to_str(self.position))
        return (None, None)

    def get_prev(self):
        if self.first_block == 'prev':
            self.get_all_infos()
        if self.cangoprev:
            if self.output_format == 'icon':
                return("idle", "\uF04A")
            else:
                return("idle", '<-')
        return(None, None)

    def get_play(self):
        if self.first_block == 'play':
            self.get_all_infos()
        if self.play or self.play is not None:
            if self.output_format == 'icon':
                if self.play is True:
                    return ("idle", '\uF04C')
                else:
                    return ("warning", '\uF04B')
            else:
                if self.play is True:
                    return ("idle", '>')
                else:
                    return ("warning", '||')
        return(None, None)

    def get_next(self):
        if self.first_block == 'next':
            self.get_all_infos()
        if self.cangonext:
            if self.output_format == 'icon':
                return("idle", "\uF04E")
            else:
                return("idle", '->')
        return(None, None)

    def get_repeat(self):
        if self.first_block == 'repeat':
            self.get_all_infos()
        if self.repeat:
            if self.output_format == 'icon':
                if self.repeat == 'Track':
                    return('idle', "\uF01D")
                elif self.shuffle is True:
                    return('idle', "\uF074")
                elif self.repeat == 'Playlist':
                    return('idle', "\uF01E")
                else:
                    return('warning', "\uF128")
        return(None, None)

