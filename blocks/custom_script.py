#!/bin/python3

import util

class Custom_script:
    def __init__(self, config):
        self.config = config
        self.error = None
        self.cmd = config['command']
        self.criticity = None
        self.to_send = ''
        self.last_tick = 0

    def on_click(self, event: str):
        pass

    def return_value(self) -> tuple:
        res, tick = util.interval_is_elapsed((self.last_tick, self.config['time_interval']))
        self.last_tick = tick
        if res is True:
            try:
                res = util.get_command(self.cmd,'\n',True)
                self.to_send = (' ').join(res)
            except:
                self.error = 'Cannot launch: ' + str(self.cmd)
        if self.error:
            return ('error', self.error)
        return (self.criticity, self.to_send)
